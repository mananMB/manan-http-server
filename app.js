const express = require("express");
const { v4: uuidv4 } = require("uuid");
const {
  ReasonPhrases,
  StatusCodes,
  getReasonPhrase,
  getStatusCode,
} = require("http-status-codes");

const path = require("path");
const app = express();
const port = 4000;

app.get("/html", (req, res) => {
  res.sendFile(path.join(__dirname, "data", "html-file.html"));
});

app.get("/json", (req, res) => {
  res.sendFile(path.join(__dirname, "data", "json-file.json"));
});

app.get("/uuid", (req, res) => {
  res.send(uuidv4());
});

app.get("/status/:code", (req, res) => {
  try {
    res.sendStatus(req.params.code);
  } catch {
    res.sendStatus(StatusCodes.UNPROCESSABLE_ENTITY);
  }
});

app.get("/delay/:time", (req, res) => {
  if (req.params.time < 0 || isNaN(req.params.time)) {
    res.sendStatus(StatusCodes.UNPROCESSABLE_ENTITY);
  } else {
    setTimeout(() => {
      res.sendStatus(StatusCodes.OK);
    }, req.params.time);
  }
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
